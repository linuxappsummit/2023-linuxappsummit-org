---
layout: page
title: "Call for locations"
permalink: /cfl/
---

## The Call for Locations is Now Open!
We are currently looking for a location to host the 2023 edition of the Linux App Summit (LAS 2023), scheduled to be held in April 2023.

LAS is an event co-hosted by [GNOME](https://www.gnome.org/) and [KDE](https://kde.org/), GNOME and KDE - the two largest end user-focused free software communities.
The summit brings together app designers, developers, and distributors from all over the world to expand the Linux application market.

**Why help us host?**
Hosting an internationally-attended summit like LAS is a great way to put your city on the map for free and open source enthusiasts, and top experts in Linux application design and development.
If you are interested in contributing to GNOME, KDE, or Free and Open Source Software in general, this is a great way to do it. Events like this serve to accelerate change and spark innovative ideas.

Previous in-person summits have been held in Portland and Denver, in the USA, and in Barcelona and Rovereto in Europe.

**How to submit a proposal**
Interested parties are hereby invited to submit a proposal to the Linux App Summit Committee. The proposal must follow the [Bid Guidelines](http://linuxappsummit.org/bids/) and the event must adhere to the [LAS Event Code of Conduct](http://linuxappsummit.org/coc/).

Please send an email with your statement of interest and later your proposal, to [appsummit@lists.freedesktop.org](mailto:info@linuxappsummit.org).

*Relevant dates*
* *1st August 2022* - statement of interest due *(this just lets us know about potential conference locations and helps you establish communication with the committee)*
* *31st August 2022* - bid proposal due *(this should adhere to the Bid Guidelines mentioned above)*
* *15th September 2022* - location announcement

After you submit your bid proposal, we might invite you to present it in more details at one of our regular meetings, or send you additional questions and requests. If you have any questions, or need help, please email the committee.

We look forward to hearing from you soon!

