---
layout: page
title: "Sponsor"
permalink: /sponsor/
---

# Sponsor LAS 2023!
The Linux Application Summit is one of the most unique conferences in the open source world. It brings together a complete vertical stack of FOSS contributors and relevant corporate organizations to work together on building a market for applications for the Linux platform. 

If successful, we will bring about a new viable alternative to closed source ecosystems and will create a marketplace for Linux apps that will greatly benefit application developers, entrepreneurs, and software companies alike.

Your sponsorship will help us build this new market for Linux applications.

*For information about sponsorship opportunities, please take a look at our [sponsorship brochure](/assets/2023LAS-Brochure.pdf) and contact us at [sponsors@linuxappsummit.org](mailto:sponsors@linuxappsummit.org).*
