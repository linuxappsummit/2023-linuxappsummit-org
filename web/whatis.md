---
layout: page
title: "What is LAS?"
permalink: /whatis
---

# What is LAS?

Applications are the foundation of the user experience and can be appreciated 
in all types of Linux environments. It is this reason that building a common 
app ecosystem is a valuable goal. At the Linux Application Summit (LAS), we 
will collaborate on all aspects aimed at accelerating the growth of the Linux 
application ecosystem.

At LAS you can attend talks, panels, and Q&As on a wide range of topics 
covering everything. From creating, packaging, and distributing apps, to 
monetization within the Linux ecosystem, designing beautiful applications, 
and more - all delivered by the top experts in each field. You will acquire 
insights into how to reach users, build a community around your app, what 
toolkits and technologies make development easier, which platforms to aim for, 
and much more.

LAS welcomes application developers, designers, product managers, user 
experience specialists, community leaders, academics, and anyone who is 
interested in the state of Linux application design and development!

With that in mind, the topics we are interested in are:

* Gaming
* Creating, packaging, and distributing applications
* Design and usability
* Commercialization
* Platform
* Linux App Ecosystem

<!-- <button class="button" onclick="window.location.href='https://linuxappsummit.org/register'">Attend LAS</button> -->

<div class="las-banner" style="background-color: #982c7e; color: white; margin-bottom: 1cm" markdown="1">
_LAS represents one of many steps towards a thriving desktop eco-system. By partnering with KDE we show the desire  to build the kind of application eco-system that demonstrates that open source and free software is important; the technology and organization we build to achieve this is valuable and necessary._

Neil McGovern, GNOME's executive director
</div>

<div class="las-banner" style="background-color: #982c7e; color: white; margin-bottom: 1cm" markdown="1">
_Over the years we have been creating great solutions that our users are using around the world. It's been when we have worked together, that we have managed to become bigger than the sum of the parts._

_Together with GNOME, counting with the collaboration of many distributions and application developers, we'll have the opportunity to work together, share our perspectives and offer the platform that the next generation of solutions will be built on._

Aleix Pol i González, KDE e.V. President
</div>

### About KDE e.V
The KDE® Community is a free software community dedicated to creating an open 
and user-friendly computing experience, offering an advanced graphical desktop, 
a wide variety of applications for communication, work, education and 
entertainment and a platform of libraries and frameworks that helps developers 
easily build new applications. We have a strong focus on finding innovative 
solutions to old and new problems, creating a dynamic atmosphere open for 
experimentation.

[https://ev.kde.org](https://ev.kde.org)

### About GNOME Foundation
The GNOME Foundation is an organization committed to supporting the 
advancement of GNOME, comprised of hundreds of volunteer developers and 
industry-leading companies. The Foundation is a member directed, 501(c)(3) 
non-profit organization that provides financial, organizational, and legal 
support to the GNOME project. The GNOME Foundation is supporting the pursuit 
of software freedom through the innovative, accessible, and beautiful user 
experience created by GNOME contributors around the world. More information 
about GNOME and the GNOME Foundation can be found at www.gnome.org and 
foundation.gnome.org. Become a friend of GNOME at https://www.gnome.org/friends/

[https://www.gnome.org/foundation](https://www.gnome.org/foundation)
