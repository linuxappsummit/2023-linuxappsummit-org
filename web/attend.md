---
layout: page
title: "Attend - Virtual and in person"
permalink: /attend/
---

# Attend LAS

This edition of LinuxAppSummit is taking place both in a virtual and in person in Rovereton, Italy. The virtual attendance will be handled all on our instance of BigBlueButton. However you choose to attend, whether in meatspace or virtually, we are happy to have you!

## Virtual Attendance

If you would like to connect with the Main Track Room, where all talks will be held and you can chat with others and ask questions to the speakers, please use the form below to connect.

<h2>Talks (Main Track)</h2>

<form action="https://matrix.to/#/#linux-app-summit-2022:matrix.org" method="post" enctype="multipart/form-data">
  <label for="name">Full Name:</label>
  <input type="text" id="name" name="name"><br><br>
  <input class="button" type="submit" value="Join" style="margin-top: 0px">
</form>

#### Socialize

The Hallway track is one of the most enjoyable parts of the conference. 

Join the room and talk with each other you need a different pin number to join the hallway track.

<h2>BoFs and Workshops</h2>

<form action="https://matrix.to/#/#linux-app-summit-2022:matrix.org" method="post" enctype="multipart/form-data">
  <label for="name">Full Name:</label>
  <input type="text" id="name" name="name"><br><br>
  <input class="button" type="submit" value="Join" style="margin-top: 0px">
</form>

## In Person Attendance

Come gather in person, observing local Covid guidelines, to engage with other Linuxers in a way we've all sorely missed. Check out our complete guide on our in-person venu, travel, etc. below.

<button class="button" onclick="window.location.href='/local'">Physically Join Us!</button>
