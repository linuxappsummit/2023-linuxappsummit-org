---
layout: page
permalink: /coc/report
toc: false
---

# LAS Code of Conduct - Report

If someone makes you feel unsafe or unwelcome, if you see someone who needs help, or if you see someone violating the Code of Conduct:
contact the LAS team at info@linuxappsummit.org or approach event staff immediately for assistance.

The Code of Conduct Team will assist you in any way they can. This may include offering practical support, issuing warnings, or removing individuals from the event.

We won’t ask you to confront anyone and we will keep your report private.

Event staff are happy to help you contact hotel/venue security or local law enforcement, provide escorts, or otherwise help you feel safe for the duration of the event.

We are here to help make this a safe and welcoming environment for you. Even if you feel like your concern is small, or if you are not sure something is an issue, we encourage you to let us know. We're good at listening to people and are often able to offer advice for seemingly minor things.

## To request help in person

Members of the Code of Conduct Team will be identified at the conference opening. You can approach them anytime and they will help you. If you are unsure who to approach, our event staff (identified by their colored shirts) can direct you to a member of the Code of Conduct Team.

When talking to you, we will make sure you are safe and cannot be overheard. Other event staff may be involved to make sure your report is managed properly. Once safe, we'll ask you to tell us what happened. We understand that this process could be upsetting for you, and we'll handle it as respectfully as possible. You can bring someone to support you.

## To request help in writing

The Code of Conduct Team can also be contacted at info@linuxappsummit.org. Team members are prepared to respond to incidents. If necessary, they will take immediate steps to support you and ensure your safety.

If you choose to provide details about an incident, a digital copy will be taken and filed in case there is a repeat offense or follow up actions are required. All information that you provide will be treated as being confidential.

When submitting information about an incident, use the subject “Code of Conduct Violation” and please try to include the following details:

• Your Information: Your full name and contact information

• Offender’s Information: Offender’s name, physically identifying information, and contact information (if known)

• Incident Description: Description of the behavior that was in violation of the Code of Conduct, including:

• Time, date, and location of incident

• Circumstances surrounding the incident

• The names of other present (if known)
