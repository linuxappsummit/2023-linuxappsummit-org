---
layout: page
title: "Location + Travel"
permalink: /local/
---

## Brno, Czech Republic

LAS 2023 takes place at Brno! This page provides more details about our location and venue, some limited guidance on where to eat and stay, some things you might want to check out if you have extra time, and logistical information.

---

## Table of Contents
**[About Brno](#about)**  
**[Venue](#venue)**  
**[Getting To/From Brno](#getting-there)**   
**[Where to Sleep/Food/Drink](#capitalism)**   

---

<div id="about"/>

## About Brno
Brno is the second-largest city in the Czech Republic, and is used to hosting several successful Free Software events such as GUADEC (2013), Akademy (2014), LibreOffice Conference (2016), and many more. Brno is a technological hub in Central Europe and the wider region. Two big and several smaller universities specializing in information technology gives Brno a large source of IT talents and many companies, like Red Hat, have opened research and development facilities in the city. This wealth of IT talents and facilities attracts foreign IT professionals who contribute to the international environment and atmosphere of Brno.

Find more information about Brno on the city's [official website](https://en.brno.cz/).

For more cultural information, check out this [website](https://www.gotobrno.cz/en/explore-brno/). 

![aerial view of Brno](/assets/brno.jpg)  
[Image by wirestock on Freepik](https://www.freepik.com/free-photo/brno-city-centre_9282811.htm)


<div id="venue"/>

## The Venue

The conference will take place at:

[Faculty of Informatics](https://www.fi.muni.cz/index.html.en)  
Masaryk University, Botanická 554/68a, 602 00 Brno

The Faculty of Informatics (FI) was founded in 1994 as the first Faculty of Informatics in the Czech Republic. Today, with a steady increase of interest to study, it provides Computer Science education at all levels of university studies for two thousand students in Czech and English. In addition to quality education, there are almost twenty laboratories where researchers and students perform leading-edge research, often in cooperation with companies located in the science and technology park directly in the FI area.

![img of FI building](/assets/fi.jpg)

<div id="getting-there"/>

## Getting to Brno

### Visa restrictions
Citizens from the Schengen area countries require only a passport to enter Czech Republic. 
Participants that require a visa, will be provided with an invitation letter from the conference organizing team.  
For more information on visa types, visit the [Ministry of Foreign Affairs of the Czech Republic website](https://www.mzv.cz/jnp/en/information_for_aliens/types_of_visas/index.html).

### By Train
- Prague: trains between Prague and Brno go hourly, 2.5 hours
- Vienna: trains between Vienna Meidling and Brno goes every hour, 2 hours
- Bratislava: from Bratislava Hlavna Stanica hourly, 1.5 hour
- Budapest: from Budapest-Keleti several trains every day, 5 hours
- Warsaw: from Warszawa Centralna several times every day (usually one connection), 7 hours
- Berlin: from Berlin Hauptbahnhof several trains every day, 7.5 hours

### By Bus  
RegioJet and FlixBus have buses from various European capitals to Brno. They often have a transfer in the Prague Florenc Bus station.

### By Plane  
Brno is about 150 km from the Vienna International Airport, 230 km from Václav Havel Airport (Prague), 144 km from Bratislava Airport, and 15 km from the local Brno Airport. 

### Vienna
**By Bus**  
[Student Agency](https://www.regiojet.com/en/services/airport-transfer/), [Gepard Express](https://www.gepard.com/), and [Flixbus operate](https://www.flixbus.com/) direct buses from Vienna’s Schwechat airport and all stop at the Grand Hotel bus station in Brno.

**By Train**  
You can also travel from Vienna airport by train – there is a [railway station in front of the arrivals hall](http://www.viennaairport.com/en/passengers/arrival__parking/s-bahn__suburban_railway). You can check the schedules and buy your ticket online here. Type Flughafen Wien in the “From” field and Brno hl. n. in the “To” field. You will have to change trains at Wien Hauptbahnhof and possibly in Břeclav as well.

- Plan of [Vienna Schwechat Airport](http://www.viennaairport.com/en/passengers/airport/airport_map)


### Prague
**By Bus**  
There are several bus companies operating buses from Prague to Brno. The yellow [Student Agency (RegioJet)](https://www.regiojet.com/) buses also leave directly from Prague’s Vaclav Havel airport (you will have to transfer at the main bus station Florenc in Prague, but one ticket is valid for the whole journey to Brno by both buses). There is a ticket office of Student Agency at Terminal 1 in Prague, but you can also buy your ticket in advance and pay by credit card – it is really convenient. 

[Flixbus](https://www.flixbus.com/), [Eurolines](https://www.eurolines.de/en/home/), and other companies operate buses from the bus station Florenc – you can get there by another bus operated by Student Agency or ask for directions at the airport. Student Agency and Flixbus will drop you off at the [Grand Hotel bus station](https://goo.gl/maps/ae526AnVBQAXo7Jg6) in Brno, while other companies usually stop at [Zvonařka bus station](https://goo.gl/maps/RjjAL3qMdGxd73P66).

**By Train**  
To get to Brno by train, you will first need to get to Prague’s main railway station – you can go by [Airport Express](https://www.cd.cz/en/dalsi-sluzby/navazna-doprava/-27548/), by [public transport](https://www.prg.aero/en/public-transport-buses) or ask for directions at the airport. You can buy your ticket online on the [website of the České dráhy company](https://www.cd.cz/en/default.htm), of the [Regiojet Company](https://novy.regiojet.cz/en), or at the train station. Please allow at least 2 hours extra to get to the railway station, purchase tickets, and find your train from the airport.

- Plan of [Vaclav Havel Airport](http://www.prg.aero/en/passenger-services/airport-maps/airport-maps/) 
- Interactive clickable plan of [Florenc Coach Station](https://florenc.cz/index.php?site=an&usite=mapy&sel=mnad&lan=en)  – the main bus station in Prague
- You can check the bus and train schedules, as well as buy the tickets [online](https://idos.idnes.cz/en/vlakyautobusy/spojeni/)

### Brno Airport and Other Local Stations
**Airport**  
There are direct flights to [Brno Airport](http://www.brno-airport.cz/en/) from some destinations. Taxi service from the airport – offered by the Foreigners.cz agency. It is highly recommended to order a taxi at least one day before ([online ordering](https://www.foreigners.cz/airport-transport)). Public transport – bus E76 (every 30 minutes) or N89 (at night). Both will take you to the “Hlavní nádraží” stop. You can check bus timetables [online](https://idos.idnes.cz/en/idsjmk/spojeni/). Type Letiště Tuřany – terminál in the “From” field and Hlavní nádraží in the “To” field. The journey takes around 20 minutes, but don’t forget to [buy a ticket at the airport](http://www.brno-airport.cz/en/parking-and-transport/public-transport/) or on the bus (you can use your debit card to pay on the bus).

**Main Railway Station (Hlavní nádraží)**  
If you are coming to the main railway station – Brno hlavní nádraží, you will only have to get to the public transport stop “Hlavní nádraží” located in front of the building .

**Grand Hotel bus Station (Autobusové nádraží u Hotelu Grand)**  
The Grand Hotel bus station – Autobusové nádraží u Hotelu Grand is located very close to the main railway station . You will only have to walk 5 minutes to get to the public transport stop “Hlavní nádraží”.  

**Zvonařka Bus Station**  
The Zvonařka bus station is located further from the city center. In order to get to the city center, you will have to take tram n. 12 and get off at the “Hlavní nádraží” stop. Alternatively, you can walk there, which takes about 10 minutes.  For more information on ways from/to Brno, and from/to the airport – see the website of the [South Moravian Integrated Public Transport System](https://www.idsjmk.cz/en/index) and the [travel planner](https://idos.idnes.cz/en/vlakyautobusy/spojeni/?&lng=E). 

You can also find other possibilities of getting to/from Brno from different cities on this useful [website](https://www.rome2rio.com/). You can check the timetables of public transport in Brno [here](https://idos.idnes.cz/en/brno/spojeni/).  

<!--
<div id="map"/>

## Map with recommended things

[![Alt text](/assets/rovereto-with-markers.svg)](https://www.openstreetmap.org/export#map=17/45.90481/11.04259)
[© Openstreetmap Contributors](https://www.openstreetmap.org/copyright)

<table>
    <tr>
        <td>
            <h3>Legend</h3>
            <h4><span style="background-color:red;font-weight:bold">Key locations</span></h4>  
                <ol start="1">
                    <li>Venue: Youth Center Smart Lab</li>
                    <li>Main Rovereto train station</li> 
                </ol>
            
            <h4><span style="background-color:yellow;font-weight:bold">Food</span></h4>  
                <ol start="3">
                    <li>Pizzeria Brione</li>
                    <li>Pizzangolo</li>  
                    <li>Osteria del Pettirosso</li>  
                    <li>DoGali Hamburgers</li>  
                    <li>Trattoria Bar Christian</li>  
                    <li>Toasteria Italiana</li>  
                    <li>Ristorante Al Lupo</li>  
                </ol>
            
            <h4><span style="background-color:pink;font-weight:bold">Dessert</span></h4>  
                <ol start="10">
                    <li>ICE Dream</li>  
                    <li>Pasticceria Zaffiro</li>  
                    <li>Gelateria Cherry</li>  
                    <li>Gelateria Marco</li>  
                    <li>Cioccolato Exquisita</li>  
                </ol>
        </td>
        <td>
            <h4><span style="background-color:blue;font-weight:bold">Beverage - Alcohol</span></h4>  
                <ol start="15">
                    <li>Black Roses Lounge</li>  
                    <li>Loco's bar</li>  
                    <li>Pub Il Druido</li>  
                </ol>
            
            <h4><span style="background-color:brown;font-weight:bold">Beverage - Coffee</span></h4>  
                <ol start="18">
                    <li>Caffetteria Bontadi</li>  
                    <li>Torrefazione Caffe</li>  
                    <li>Sun Coffee</li>  
                </ol>
            
            <h4><span style="background-color:grey;font-weight:bold">Activities</span></h4>  
                <ol start="21">
                    <li>Museum of Modern and Contemporary Art</li>  
                    <li>Museo Storico Italiano della Guerra</li>  
                    <li>Bell of the Fallen/Campana dei Caduti</li>  
                </ol>
        </td>
    </tr>
</table>
-->


<div id="capitalism"/>

## Where to Stay

The following structures have different pricing ranges and offers.  
All of them are reachable by public transportation and walking from the venue.

#### Avanti Hotel
- ~ 95 EUR standard room /  ~105 EUR (including wellness) - includes breakfast
- 4-stars
- 10 minutes to MUNI (750m)
- https://www.hotelavanti.cz/en/


#### Cosmopolitan Bobycenter
- ~ 180 EUR twin-room / single-room - includes breakfast
- 4-stars
- 13 minutes to MUNI (1km)
- https://www.hotelcosmopolitan.cz/en/


#### Hotel Sono Brno
- ~84 EUR twin-room / ~73 EUR single-room - includes breakfast
- 4-stars
- 14 minutes to MUNI (1,2km)
- https://www.hotel-brno-sono.cz/en


#### Hotel Passage 
- ~ 127 EUR twin-room / ~115 EUR single-room - includes breakfast
- 4-stars
- 16 minutes to MUNI (1,2km)
- https://www.hotelpassage.eu/en/


#### Hotel Continental
- ~123 EUR twin-room / ~90 EUR single-room - includes breakfast
- 4-stars
- 17 minutes (1,4km)
- https://www.continentalbrno.cz/en/


#### EFI SPA Hotel Superior
- ~ 125 EUR twin-room / ~117 EUR single-room - includes breakfast
- 4-stars
- 22 minutes (1,8km)
- https://www.efihotel.cz/en/efispahotel


#### Hotel Amphone
- ~50 EUR single room
- 3 stars
- 10 minutes to MUNI (1km)
- https://amphone.hotel.cz/



   

<!--
<div id="activities"/>

## Things to do in Rovereto

-->

