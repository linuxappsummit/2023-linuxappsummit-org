---
layout: page
title: "Call for proposals"
permalink: /cfp/
---
<!--
<hr/>
<div class="las-banner" style="background-color: orange; color: black;">
    <h2>The call for papers has closed.</h2>
</div>
<hr/>
-->

# LAS 2023 Call for proposals

The Linux App Summit (LAS) is designed to accelerate the growth of the Linux application ecosystem by bringing together everyone involved in creating a great Linux application user experience.

Keynote talks will be 60', regular talks will be 40', and lightning talks will be 10'. You can also hold a BOF or workshop (time slots TBD based on need). In any format, come and share exciting content!

**We are only accepting in-person presentations and all talks will be given in English.**


## ECOSYSTEM GROWTH

The growth of Linux-based hardware offers more choices in the market and indicates further investment into the Linux app space. App Stores are offering thousands of applications and games for the Linux platform and open source is growing necessity in tech.

How can we ensure continued growth for the Linux app ecosystem? Are there areas of opportunity we should explore? How can we build sustainability into our ecosystem? How do you keep your users engaged?

## GAMING

With the contributions from Valve (Steam and Proton), and with community gaming initiatives such as Lutris/Wine, gaming on Linux is continuing to gain momentum.

What direction does the free desktop need to take to ensure gaming on Linux is a success? What project are happening to push gaming on Linux forward?

## INNOVATION

Important technologies that you use on a daily basis (maybe without realizing it), have been developed within the Linux app ecosystem. Discuss with us how to push the boundaries of what we are offering right now and reach more users than ever before.

What does the Linux app ecosystem need to get to the top? What are we missing? Are there new technologies we should be embracing?

## PEOPLE & COMMUNICATIONS

While the Linux app ecosystem has been largely shaped by open source communities, we believe there's space for everyone. Making sure both businesses and communities alike can thrive in the ecosystem is vital for the platform to be sustainable over time. It is important for people to be able to make a living from the work they do for Linux users.

How can we make sure companies find their niche in the ecosystem? How can we help communities work together to create and support end-user apps? How can we keep people around in the long-run?

## PLATFORM DIVERSITY

The Linux platform’s greatest strength is the wealth of diversity in our ecosystem. There are toolkits and software languages for every purpose. Whether you want to develop a game, design an application, or even build a website, the app ecosystem has the tools to make that happen.

Are you working on a technology that enables cross-platform distribution? Do you have ideas for how we can enable platform diversity? How should we evolve to reach your users?


# Important Dates:

* Call for Proposals opens: January 20th
* Call for Proposals closes: February 18th
* Speakers announced: March 1st


<button class="button" onclick="window.location.href='https://conf.linuxappsummit.org/event/5/abstracts/#submit-abstract'">Submit your talk</button>
