---
layout: page
title: "Register"
permalink: /register/
---
# Registration

The Linux App Summit attendance is free of charge, but you must register to attend the event.

Registration is open now, for both virtual and in-person attendance!

<button class="button" onclick="window.location='https://conf.linuxappsummit.org/event/5/registrations/'">Register Here!</button><!-- <button class="button" onclick="window.location.href='https://conf.linuxappsummit.org/event/5/timetable/#all'">Timetable</button>-->

### Note: How to Watch and Participate Remotely
To watch the sessions and participate, you'll need to login to the conference platform via the password-protected link provided when you register.

Videos from LAS will also be live streamed on the [LAS YouTube channel](https://www.youtube.com/channel/UCjSsbz2TDxIxBEarbDzNQ4w).

We also encourage attendees to join the [LAS Telegram channel](https://t.me/+UmPHi4WPWunYzXjc) to chat with us, and follow LAS on <a href="https://twitter.com/LinuxAppSummit">Twitter @linuxappsummit</a>. Remember to use our hashtag: #LAS2023

See you soon!
